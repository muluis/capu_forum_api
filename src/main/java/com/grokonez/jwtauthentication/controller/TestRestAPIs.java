package com.grokonez.jwtauthentication.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grokonez.jwtauthentication.security.jwt.JwtAuthEntryPoint;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class TestRestAPIs {
    private static final Logger logger = LoggerFactory.getLogger(TestRestAPIs.class);
	
	
	@GetMapping("/api/test/user")
	public String userAccess() {
		return ">>> User Contents!";
	}

	@GetMapping("/api/test/pm")
	public String projectManagementAccess() {
		return ">>> Project Management Board";
	}
	
	@GetMapping("/api/test/admin")
	public String adminAccess() {
		return ">>> Admin Contents";
	}
}