package com.boeing.tdmrepository.common.util;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.boeing.tdmrepository.common.constants.CommonConstants;

public class TestReg {
	static Scanner usrInput = new Scanner(System.in);
	public static void main(String[] args) {
		while(true){
			alphaNumericIncrement(usrInput.nextLine().toUpperCase());
		}

	}
	public static String alphaNumericIncrement(String alphaNumeric) {
	    Pattern compile = Pattern.compile("^(.*?)([9Z]*)$");
	    Matcher matcher = compile.matcher(alphaNumeric);
	    String left=CommonConstants.EMPTY_STRING;
	    String right=CommonConstants.EMPTY_STRING;
	    String number=CommonConstants.EMPTY_STRING;
	    if(matcher.matches()){
	         left = matcher.group(1);
	         right = matcher.group(2);
	    }
	    number = !left.isEmpty() ? Long.toString(Long.parseLong(left, 36) + 1,36):CommonConstants.EMPTY_STRING;
	    number += right.replace("Z", "A").replace("9", "0");
//	    if (number.length() == 4)
//	    	number = "0" + number;
	    
	    System.out.println(number.toUpperCase());
	    
	    return number.toUpperCase();
	}
}
